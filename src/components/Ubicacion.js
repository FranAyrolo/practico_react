import React from 'react';
import apiGobBase from "../config/axios";

const Ubicacion = () => {
    
    const listaRandom = ["chubut", "uruguay", "cordoba"];
    listaRandom[2] = "Cordoba";

    const consultAPI = async() => {
        const respuesta = await apiGobBase("/provincias");
        return respuesta.data;
        /*
        const APIresponse = await fetch("https://apis.datos.gob.ar/georef/api/provincias");
        const data = await APIresponse.json()
        const provs = data.provincias;
        return provs;
        */
    }
    
    const provincias = consultAPI();
    console.log(provincias);

    return (
        <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
            </button>
            <div className="dropdown-menu" aria-labelledby="dropdownMenu2">
                <button className="dropdown-item" type="button">Action</button>
                <button className="dropdown-item" type="button">Another action</button>
                <button className="dropdown-item" type="button">Something else here</button>
            </div>
        </div>
        
    );

}

export default Ubicacion;
