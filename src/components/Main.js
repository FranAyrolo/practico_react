import React, {useState} from 'react';

const Main = () => {
    
    const [datosUsuario, changeDatosUsuario] = useState({
        nombre: "",
        apellido: "",
        fechaNacimiento: null
    });
    const {nombre, apellido, fechaNacimiento} = datosUsuario;

    const onChange = (e) => {
        changeDatosUsuario({
            ...datosUsuario, [e.target.name] : e.target.value
        })
    }

    const onSubmit = (e) => {
        e.preventDefault();
    }

    return (
        <div className="">
            <h1> Iniciar Sesión </h1>
            <form
                onSubmit={onSubmit}
            >
                <div className="">
                    <label htmlFor="nombre"> Nombre </label>
                    <input
                        type="nombre"
                        id="nombre"
                        name="nombre"
                        placeholder="tu nombre"
                        value={nombre}
                        onChange={onChange}
                        />
                </div>

                <div className="">
                    <label htmlFor="apellido"> Apellido </label>
                    <input
                        type="apellido"
                        id="apellido"
                        name="apellido"
                        placeholder="contraseña"
                        value={apellido}
                        onChange={onChange}
                        />
                </div>

                <div className="">
                    <label htmlFor=""> Fecha de Nacimiento </label>
                    <input
                        type="date"
                        id="fechaNacimiento"
                        name="fechaNacimiento"
                        value={fechaNacimiento}
                        onChange={onChange}
                        />
                </div>

                <div className="">
                    <input type="submit" className=""
                        value="Iniciar Sesión" />
                </div>
            </form>
            
        </div>
    );

}

export default Main;
