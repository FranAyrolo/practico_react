import axios from "axios";

const apiGobBase = axios.create({
    baseURL: "https://apis.datos.gob.ar/georef/api/",
});

export default apiGobBase;