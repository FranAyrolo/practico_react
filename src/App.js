import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "./components/Header";
import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Main from "./components/Main";
import Ubicacion from "./components/Ubicacion";
import Autocompletado from "./components/Autocompletado";
import MuestraDatos from "./components/MuestraDatos";

function App() {
    return (
        <Router>
            <Header/>
            <Switch>
                <Route exact path="/" component={Main} />
                <Route exact path="/ubicacion" component={Ubicacion} />
                <Route exact path="/autofill" component={Autocompletado} />
                <Route exact path="/muestra-datos" component={MuestraDatos} />
            </Switch>
        </Router>
    );
}

export default App;
